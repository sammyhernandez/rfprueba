*** Settings ***
Library		String
Library		SeleniumLibrary


*** Variables ***
${browser}		chrome	
${homepage}		automationpractice.com/index.php
${scheme}		http
${testURL}		${scheme}://${homepage}


*** Keywords ***
Open HomePage
	Open browser	${testURL}	${browser}


*** Test Cases ***
Caso-01 Hacer click en contenedores
	Open Homepage
	Set Global Variable 	@{nombresDeContenedores}	//*[@id="homefeatured"]/li[1]/div/div[2]/h5/a 	//*[@id="homefeatured"]/li[2]/div/div[2]/h5/a 	//*[@id="homefeatured"]/li[3]/div/div[2]/h5/a 	//*[@id="homefeatured"]/li[4]/div/div[2]/h5/a 	//*[@id="homefeatured"]/li[5]/div/div[2]/h5/a 	//*[@id="homefeatured"]/li[6]/div/div[2]/h5/a 	//*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
	:FOR	${contenedor1}	IN 	@{nombresDeContenedores}
	\	Click Element		xpath=${contenedor1}
	\	Wait Until Element Is Visible 	//*[@id="product_condition"]/label
	\	Click Element	//*[@id="header_logo"]/a/img
	Close Browser